﻿using System;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace WineClassification
{
    class WineClassifier
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Begin back-propagation demo");
            int numInput = 13; //number features
            int numHidden = 12; 
            int numOutput = 3; //number of wineries to classify
            
            double[][] trainData = ReadInData("Wine_train.xlsx"); // Read in training data
            double[][] testData = ReadInData("Wine.xlsx"); // Read in test data
          
            //Instantiate the neural network
            BackPropNeuralNetwork neuralNetwork = new BackPropNeuralNetwork(numInput, numHidden, numOutput);

            //back-propagation parameter values assigned
            int maxEpochs = 100;
            double learnRate = 0.2;
            Console.WriteLine(String.Format("Setting maxEpochs = {0}",maxEpochs));
            Console.WriteLine(String.Format("Setting learnRate = {0}", learnRate));

            Console.WriteLine("Starting training");
            neuralNetwork.Train(trainData, maxEpochs, learnRate,20);
            Console.WriteLine("Done");

            //calculate the accuracy on both the test data and the training data
            double trainAcc = neuralNetwork.RunTestData(trainData);
            Console.WriteLine(String.Format("Accuracy on training data = {0}", trainAcc));
            double testAcc = neuralNetwork.RunTestData(testData);
            Console.WriteLine(String.Format("Accuracy on testing data = {0}", testAcc));
            Console.WriteLine("end of back-propagation demo");

            Console.ReadLine();
        }
        
        public static double[][] ReadInData(String docName)
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(docName, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                var rows = sheetData.Elements<Row>();
                List<Row> rowList = rows.ToList() ;
                rowList.RemoveAt(0);
                double[][] result = new double[rowList.Count()][]; // allocate return-result
                string text;
                int i = 0;
                foreach (Row r in rowList)
                {
                    var elements = r.Elements<Cell>();
                    double[] currentRow = new double[elements.Count()];

                    int j = 0;
                    foreach (Cell c in elements)
                    {
                        if (c.CellValue != null)
                        {
                            text = c.CellValue.Text;
                            double curValue = Convert.ToDouble(text);
                            currentRow[j] = NormalizeValue(j, curValue);
                            //  Console.Write(text + ", ");
                            j++;
                        }
                    }
                    result[i] = currentRow;
                    //Console.WriteLine();
                    i++;
                }
                return result;
            }
        }

        static double NormalizeValue(int index, double value)
        {
            double returnVal = 0;
            double max = 10;
            double min = -10;
            switch (index)
            {
                case 0:
                    returnVal = ((value - 11.03) / (14.03 - 11.03))* (max - min) - Math.Abs(min);
                    break;
                case 1:
                    returnVal = ((value - 0.74) / (5.8 - 0.74)) * (max - min) - Math.Abs(min);
                    break;
                case 2:
                    returnVal = ((value - 1.7) / (3.23 - 1.7)) * (max - min) - Math.Abs(min);
                    break;
                case 3:
                    returnVal = ((value - 11.4) / (30 - 11.4)) * (max - min) - Math.Abs(min);
                    break;
                case 4:
                    returnVal = ((value - 70) / (162 - 70)) * (max - min) - Math.Abs(min);
                    break;
                case 5:
                    returnVal = ((value - 0.98) / (3.88 - 0.98)) * (max - min) - Math.Abs(min);
                    break;
                case 6:
                    returnVal = ((value - 0.34) / (5.08 - 0.34)) * (max - min) - Math.Abs(min);
                    break;
                case 7:
                    returnVal = ((value - 0.13) / (0.66 - 0.13)) * (max - min) - Math.Abs(min);
                    break;
                case 8:
                    returnVal = ((value - 0.42) / (3.58 - 0.42)) * (max - min) - Math.Abs(min);
                    break;
                case 9:
                    returnVal = ((value - 1.28) / (13 - 1.28)) * (max - min) - Math.Abs(min);
                    break;
                case 10:
                    returnVal = ((value - 0.48) / (1.71 - 0.48)) * (max - min) - Math.Abs(min);
                    break;
                case 11:
                    returnVal = ((value - 1.27) / (4 - 1.27)) * (max - min) - Math.Abs(min);
                    break;
                case 12:
                    returnVal = ((value - 278) / (1680 - 278)) * (max - min) - Math.Abs(min);
                    break;
                default:
                    returnVal = value;
                    break;
            }
            return returnVal;
        }
    }
}