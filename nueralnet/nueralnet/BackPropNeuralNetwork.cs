﻿using System;
using System.Linq;

namespace WineClassification
{
    class BackPropNeuralNetwork
    {
        private int InputNodes;
        private int HiddenNodes;
        private int OutputNodes;

        private double[] InputSamples;
        private double[,] InputToHiddenWeights;
        private double[] HiddenBiases;
        private double[] HiddenOutputs;

        private double[,] HiddenToOutputWeights;
        private double[] OutputBiases;
        private double[] ClassificationOutputs;

        private double Derivative = 0.0;
        private double ErrorSignal = 0.0;
        private double[] OutputNodeSignals;

        private double[,] HiddenToOutputGradient;
        private double[] OutputBiasGradient;

        private double[] HiddenNodeSignals;

        private double[,] InputToHiddenGradient;

        private double[] HiddenBiasGradient;

        private double LearnRate;

        private Random RandomNumber;

        public BackPropNeuralNetwork(int numInput, int numHidden, int numOutput)
        {
            this.InputNodes = numInput;
            this.HiddenNodes = numHidden;
            this.OutputNodes = numOutput;
            this.HiddenToOutputGradient = new double[numHidden, numOutput];
            this.OutputBiasGradient = new double[numOutput];
            this.HiddenNodeSignals = new double[numHidden];
            this.InputToHiddenGradient = new double[numInput, numHidden];

            this.InputSamples = new double[numInput];

            this.InputToHiddenWeights = new double[numInput, numHidden];
            this.HiddenBiases = new double[numHidden];
            this.HiddenOutputs = new double[numHidden];
            this.OutputNodeSignals = new double[numOutput];
            this.HiddenToOutputWeights = new double[numHidden, numOutput];
            this.OutputBiases = new double[numOutput];
            this.ClassificationOutputs = new double[numOutput];
            this.HiddenBiasGradient = new double[numHidden];

            this.RandomNumber = new Random(0);
            this.SetInputToHiddenWeights();
            this.SetHiddenBiases();
            this.SetHiddenToOutputWeights();
            this.SetOutputBiases();
        }

        private void SetInputToHiddenWeights()
        {
            for (int currentInputNode = 0; currentInputNode < this.InputNodes; ++currentInputNode)
            {
                for (int currentHiddenNode = 0; currentHiddenNode < this.HiddenNodes; ++currentHiddenNode)
                {
                    InputToHiddenWeights[currentInputNode, currentHiddenNode] = (0.01) * RandomNumber.NextDouble();
                }
            }
        }

        private void SetHiddenBiases()
        {
            for (int currentHiddenNode = 0; currentHiddenNode < HiddenNodes; ++currentHiddenNode)
            {
                HiddenBiases[currentHiddenNode] = (0.01) * RandomNumber.NextDouble();
            }
        }

        private void SetHiddenToOutputWeights()
        {
            for (int currentHiddenNode = 0; currentHiddenNode < this.HiddenNodes; ++currentHiddenNode)
            {
                for (int currentOutputNode = 0; currentOutputNode < this.OutputNodes; ++currentOutputNode)
                {
                    this.HiddenToOutputWeights[currentHiddenNode, currentOutputNode] = (0.01) * this.RandomNumber.NextDouble();
                }
            }
        }

        private void SetOutputBiases()
        {
            for (int currentOutputNode = 0; currentOutputNode < this.OutputNodes; ++currentOutputNode)
            {
                this.OutputBiases[currentOutputNode] = (0.01) * this.RandomNumber.NextDouble();
            }
        }
        
        public double[] GetWeights()
        {
            int numWeights = (this.InputNodes * this.HiddenNodes) +
              (this.HiddenNodes * this.OutputNodes) + this.HiddenNodes + this.OutputNodes;
            double[] result = new double[numWeights];
            int currentResult= 0;
            for (int i = 0; i < this.InputToHiddenWeights.GetLength(0); ++i)
                for (int j = 0; j < this.InputToHiddenWeights.GetLength(1); ++j)
                {
                    result[currentResult] = this.InputToHiddenWeights[i, j];
                    currentResult++;
                }
            for (int currentHiddenBias = 0; currentHiddenBias < this.HiddenBiases.Length; ++currentHiddenBias)
            {
                result[currentResult] = this.HiddenBiases[currentHiddenBias];
                currentResult++;
            }
            for (int i = 0; i < HiddenToOutputWeights.GetLength(0); ++i)
            {
                for (int j = 0; j < HiddenToOutputWeights.GetLength(1); ++j)
                {
                    result[currentResult] = HiddenToOutputWeights[i, j];
                    currentResult++;
                }
            }
            for (int currentOutputBias = 0; currentOutputBias < OutputBiases.Length; ++currentOutputBias)
            {
                result[currentResult] = OutputBiases[currentOutputBias];
                currentResult++;
            }
            return result;
        }

        public double[] ComputeOutputs(double[] valuesToClassify)
        {
            double[] hiddenSums = new double[HiddenNodes]; 
            double[] outputSums = new double[OutputNodes]; 

            Array.Copy(valuesToClassify,this.InputSamples, valuesToClassify.Length);

            for (int currentHiddenNode = 0; currentHiddenNode < this.HiddenNodes; ++currentHiddenNode)
            {// compute input to hidden sum of weights * inputs
                for (int currentInputNode = 0; currentInputNode < this.InputNodes; ++currentInputNode)
                {
                    hiddenSums[currentHiddenNode] += this.InputSamples[currentInputNode] * this.InputToHiddenWeights[currentInputNode, currentHiddenNode]; // note +=
                }
                hiddenSums[currentHiddenNode] += this.HiddenBiases[currentHiddenNode]; //add biases to hidden sums do this in one loop after hidden sums are computed to reduce 
                                                                                       //execution complexity

                this.HiddenOutputs[currentHiddenNode] = Math.Tanh((hiddenSums[currentHiddenNode])); // Compute tanh here
            }

            for (int currentOutputNode = 0; currentOutputNode < this.OutputNodes; ++currentOutputNode)   // compute hidden to output sum of weights * hOutputs
            {
                for (int currentHiddenNode = 0; currentHiddenNode < this.HiddenNodes; ++currentHiddenNode)
                {
                    outputSums[currentOutputNode] += this.HiddenOutputs[currentHiddenNode] * this.HiddenToOutputWeights[currentHiddenNode, currentOutputNode];
                }
                outputSums[currentOutputNode] += this.OutputBiases[currentOutputNode]; //add output biases to output sums do this in one loop after output sums are computed to reduce 
                                                                                       //execution complexity
            }

            double[] softOut = Softmax(outputSums);

            Array.Copy(softOut, this.ClassificationOutputs, softOut.Length);

            return this.ClassificationOutputs;
        }

        private static double[] Softmax(double[] outputSums)
        {
            double sum = 0.0;
            for (int currentSum = 0; currentSum < outputSums.Length; ++currentSum)
            {
                sum += Math.Exp(outputSums[currentSum]);
            }

            double[] result = new double[outputSums.Length];
            for (int currentSum = 0; currentSum < outputSums.Length; ++currentSum)
            {
                result[currentSum] = Math.Exp(outputSums[currentSum]) / sum;
            }

            return result; 
        }

        public void Train(double[][] trainData, int maxEpochs,
            double learnRate, int epochIntervals)
        {                              
            int epoch = 0;
            double[] inputValues = new double[InputNodes];
            double[] targetValues = new double[InputNodes];
            int errInterval = maxEpochs / epochIntervals;
            this.LearnRate = learnRate;

            int[] sequence = Enumerable.Range(0, trainData.Length).ToArray();

            while (epoch < maxEpochs)
            {
                ++epoch;

                //Calculate the mean square error
                if (epoch % errInterval == 0 && epoch < maxEpochs)
                {
                    double trainErr = Error(trainData);
                    Console.WriteLine(String.Format("epoch = {0} error = {1}", epoch, trainErr));
                }

                //Shuffle around the Data
                Random random = new Random();
                sequence = sequence.OrderBy(x => random.Next()).ToArray();

                for (int currentSample = 0; currentSample < trainData.Length; ++currentSample)
                {
                    int index = sequence[currentSample];
                    Array.Copy(trainData[index], inputValues, this.InputNodes);
                    Array.Copy(trainData[index], this.InputNodes, targetValues, 0, this.OutputNodes);
                    ComputeOutputs(inputValues);

                    ComputeOutputSignalsAndBiases(targetValues);

                    ComputeHiddenToOutputWeightsAndBiases();

                    ComputeInputHiddenWeightGradientsAndBiases();

                }
            } 
        }
        
        private void UpdateHiddenBiases(int currentHiddenNode)
        {
            //update hidden bias for given node
            double hiddenBiasDelta = HiddenBiasGradient[currentHiddenNode] * this.LearnRate;
            HiddenBiases[currentHiddenNode] += hiddenBiasDelta;
        }


        private void ComputeOutputSignalsAndBiases(double[] tValues)
        {
            for (int currentOutputNode = 0; currentOutputNode < this.OutputNodes; ++currentOutputNode)
            {
                this.ErrorSignal = tValues[currentOutputNode] - this.ClassificationOutputs[currentOutputNode];
                this.Derivative = (1 - this.ClassificationOutputs[currentOutputNode]) * this.ClassificationOutputs[currentOutputNode];
                this.OutputNodeSignals[currentOutputNode] = this.ErrorSignal * this.Derivative;
                this.OutputBiasGradient[currentOutputNode] = this.OutputNodeSignals[currentOutputNode];
                this.UpdateOutputBias(currentOutputNode);
            }

        }          
        
        private void ComputeHiddenToOutputWeightsAndBiases()
        {
            for (int currentHiddenNode = 0; currentHiddenNode < HiddenNodes; ++currentHiddenNode)
            {
                for (int currentOutputNode = 0; currentOutputNode < OutputNodes; ++currentOutputNode)
                {
                    this.HiddenToOutputGradient[currentHiddenNode, currentOutputNode] = this.OutputNodeSignals[currentOutputNode] * this.HiddenOutputs[currentOutputNode];
                }

                this.ComputeHiddenNodeSignals(currentHiddenNode);

                this.HiddenBiasGradient[currentHiddenNode] = this.HiddenNodeSignals[currentHiddenNode]; // Compute Hidden Node Bias Gradient

                this.UpdateHiddenBiases(currentHiddenNode);

                this.UpdateHiddenToOutputWeights(currentHiddenNode);
            }
        }

        private void ComputeOutputBiasGradient()
        {
            for (int currentOutputNode = 0; currentOutputNode < OutputNodes; ++currentOutputNode)
                OutputBiasGradient[currentOutputNode] = OutputNodeSignals[currentOutputNode];
        }

        private void ComputeHiddenNodeSignals(int currentHiddenNode)
        {
                this.Derivative = (1 + this.HiddenOutputs[currentHiddenNode]) * (1 - this.HiddenOutputs[currentHiddenNode]); //tanh
                double sum = 0.0;
                for (int currentOutputNode = 0; currentOutputNode < this.OutputNodes; ++currentOutputNode)
                {
                    sum += this.OutputNodeSignals[currentOutputNode] * this.HiddenToOutputWeights[currentHiddenNode,currentOutputNode];
                }
                this.HiddenNodeSignals[currentHiddenNode] = this.Derivative * sum;
           
        }

        private void ComputeInputHiddenWeightGradientsAndBiases()
        {
            for (int currentInputNode = 0; currentInputNode < this.InputNodes; ++currentInputNode)
            {
                for (int currentHiddenNode = 0; currentHiddenNode < this.HiddenNodes; ++currentHiddenNode)
                {
                    InputToHiddenGradient[currentInputNode, currentHiddenNode] = HiddenNodeSignals[currentHiddenNode] * InputSamples[currentInputNode];
                }

                UpdateInputHiddenWeights(currentInputNode);

            }
        }

        private void UpdateInputHiddenWeights(int currentInputNode)
        {
                for (int currentHiddenNode = 0; currentHiddenNode < HiddenNodes; ++currentHiddenNode)
                {
                    double delta = this.InputToHiddenGradient[currentInputNode ,currentHiddenNode] * this.LearnRate;
                    this.InputToHiddenWeights[currentInputNode , currentHiddenNode] += delta;

                }
        }

        private void UpdateHiddenToOutputWeights(int currentHiddenNode)
        {
                for (int currentOutputNode = 0; currentOutputNode < OutputNodes; ++currentOutputNode) //output node loop
                {
                    double delta = HiddenToOutputGradient[currentHiddenNode, currentOutputNode] * this.LearnRate;
                    HiddenToOutputWeights[currentHiddenNode, currentOutputNode] += delta;
                }
        }

        private void UpdateOutputBias(int currentOutputNode)
        {
                double outputBiasDelta = this.OutputBiasGradient[currentOutputNode] * this.LearnRate;
                OutputBiases[currentOutputNode] += outputBiasDelta;  //update output node biases
        }

        private double Error(double[][] trainData)
        {      
            //mean squared error per training item
            double sumSquaredError = 0.0;
            double[] inputValues = new double[this.InputNodes]; // first numInput values in trainData
            double[] targetValues = new double[OutputNodes]; // last numOutput values
            
            for (int trainNumber = 0; trainNumber < trainData.Length; ++trainNumber)
            {
                Array.Copy(trainData[trainNumber], inputValues, this.InputNodes);
                Array.Copy(trainData[trainNumber], this.InputNodes, targetValues, 0, OutputNodes); // get target values
                double[] outputValues = this.ComputeOutputs(inputValues); // outputs using current weights
                for (int currentOutputNode = 0; currentOutputNode < OutputNodes; ++currentOutputNode)
                {
                    double error = targetValues[currentOutputNode] - outputValues[currentOutputNode];
                    sumSquaredError += error * error;
                }
            }
            return sumSquaredError / trainData.Length;
        }

        public double RunTestData (double[][] testData)
        {
            // percentage correct
            int numCorrect = 0;
            int numWrong = 0;
            double[] inputValues = new double[this.InputNodes];
            double[] targetValues = new double[OutputNodes]; 
            double[] computedValues; 

            for (int currentTestData = 0; currentTestData < testData.Length; ++currentTestData)
            {
                Array.Copy(testData[currentTestData], inputValues, this.InputNodes); // get input values
                Array.Copy(testData[currentTestData], this.InputNodes, targetValues, 0, OutputNodes); // get target values
                computedValues = this.ComputeOutputs(inputValues);
                int maxIndex = MaxIndex(computedValues); // get max computed value
                int targetMaxIndex = MaxIndex(targetValues); //get max target value

                Console.WriteLine(String.Format("ExpectedOutput: {0} ActualOutput:{1}", targetMaxIndex, maxIndex));

                if (maxIndex == targetMaxIndex)
                    ++numCorrect;
                else
                    ++numWrong;
            }
            return (numCorrect * 1.0) / (numCorrect + numWrong);
        }

        private int MaxIndex(double[] vector)
        {
            // Find the maximum
            double maximum = vector.Max();

            // return maximum index
            return Array.IndexOf(vector, maximum);
        }
    }
}
